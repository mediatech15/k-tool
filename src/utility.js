let { getDataHome, getHomeFolder } = require('platform-folders');
let path = require('path');
let fs = require('fs');
let glob = require('glob');
const { exec } = require('child_process');
const { cwd } = require('process');

let configPath = path.join(getDataHome(), 'ktool', 'config.json');

let defaultConfig = {
    'nuget.cache': path.join(getHomeFolder(), '.nuget'),
};

try {
    var appSettings = JSON.parse(fs.readFileSync(configPath));
    _validateJsonKeys();
} catch (error) {
    if (error.code !== 'ENOENT') {
        throw error;
    }
    _createConfig();
}


module.exports = {
    test: function (args) {
        console.log(args);
    },

    config: function (args) {
        _updateConfig(_getKey(args), _getValue(args));
    },

    tunables: function () {
        console.table(appSettings);
    },

    clearBins: function () {
        _clearBins();
    },

    cleanBranches: function () {
        _cleanBranches();
    }
};

function _createConfig() {
    if (!fs.existsSync(path.dirname(configPath))) {
        fs.mkdirSync(path.dirname(configPath), true);
    }
    fs.writeFileSync(configPath, JSON.stringify(defaultConfig));
    appSettings = defaultConfig;
}

function _writeConfig() {
    fs.writeFileSync(configPath, JSON.stringify(appSettings));
}

function _updateConfig(key, value) {
    appSettings[key] = value;
    _writeConfig();
}

function _getKey(args) {
    return args.tunable;
}

function _getValue(args) {
    return args.new;
}

function _validateJsonKeys() {
    for (const [key, value] of Object.entries(defaultConfig)) {
        if (appSettings[key] == null) {
            appSettings[key] = value;
        }
    }
    _writeConfig();
}

function _clearBins() {
    options = {
        cwd: cwd()
    };
    console.log('Removing...');
    let r = [];
    glob("**/bin/", options, function (er, files) {
        for (f of files) {
            console.log(f);
            fs.rmdirSync(path.join(cwd(), f), { recursive: true });
        }
    });
    glob("**/obj/", options, function (er, files) {
        for (f of files) {
            console.log(f);
            fs.rmdirSync(path.join(cwd(), f), { recursive: true });
        }
    });
}

function _cleanBranches() {
    exec(`powershell -Command {git branch -vv | where {$_ -match '\[origin/.*: gone\]'} | foreach {$_.split(" ", [StringSplitOptions]'RemoveEmptyEntries')}}`, { cwd: cwd() }, (error, stdout, stderr) => {
        if (error) {
            console.error(`exec error: ${error}`);
            return;
        }
        console.log(`${stdout}`);
        console.error(`${stderr}`);
    });
}