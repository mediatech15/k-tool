#!/usr/bin/env node
'use strict';

// Imports
let { Command } = require('commander');
let utility = require('./utility');
const chalk = require('chalk');

// Globals
let program = new Command('ktool');
let globalOpts = program.opts();
program.version(require('../package.json').version, '-v, --version', 'Tool Version');
program.allowExcessArguments(false);
program.showHelpAfterError();
program.description(chalk.whiteBright('Kyle Mason Dev Tool Help'));
program.addHelpText('beforeAll', chalk.whiteBright('Kyle Mason Dev Tool Help'));
program.addHelpText('beforeAll', chalk.yellowBright(`Version: ${require('../package.json').version}\n`));
program.addHelpText('afterAll', ' ');
program.option('-d, --debug', 'Debug enable', false);

// Command Definition

let test = new Command('test')
    .action(() => {
        utility.test(globalOpts);
    });
let config = new Command('config')
    .option('-t, --tunable <tunable>', 'Item to update')
    .option('-n, --new <value>', 'New value to store')
    .description('Used for setting tool configuration parameters.')
    .action(() => {
        if (config.opts().tunable) {
            utility.tunables();
        } else {
            utility.config(config.opts());
        }
    });
let clean = new Command('clean')
    .option('--nuget', 'Remove nuget cache')
    .option('--bin', 'Remove bin folders')
    .option('--obj', 'Remove obj folders')
    .option('--dead-branches', 'Remove dead branches in git. Will remove all branches not on remote')
    .option('--node-modules', 'Remove node modules folder')
    .description('This command is for cleaning up folders in the workspace or globally. You need to specify at least one option.')
    .action(() => {
        if (Object.keys(clean.opts()).length == 0) {
            clean.outputHelp();
        } else {
            if (clean.opts().bin) {
                utility.clearBins();
            } else if (clean.opts().nuget) {

            } else if (clean.opts().obj) {

            } else if (clean.opts().deadBranches) {
                utility.cleanBranches();
            } else if (clean.opts().nodeModules) {

            }
        }
    });




// add commands
//program.addCommand(test);
program.addCommand(config);
program.addCommand(clean);

// Main Parse
program.parse(process.argv);
if (globalOpts.debug === true) {
    console.log('Global Options:', globalOpts);
    console.log('Program Args:  ', program.args);
}