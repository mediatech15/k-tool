# K-Tool

## Command structure 
`ktool command [options]`

## Global Options
|option|description|
|---|---|
|`-d, --debug`|Enable debug output|

## Tool tunables
|tunable|values|default|description|
|---|---|---|---|
|`nuget.cache`|string|`{user}\.nuget`|nuget cache folder|


## Commands
### config
`ktool config -t <tunable> -n <value...> [-d]`
|option|description|
|---|---|
|`-t, --tunable`|item to update in config (from tunables)|
|`-n, --new`|new value to set|

Updates or reads config items for the tool

### clean
`ktool clean [options] [-d]`
|option|description|
|---|---|
|`--nuget`|Remove nuget cache|
|`--bin`|Remove bin folders|
|`--obj`|Remove obj folders|
|`--dead-branches`|Remove dead branches in git. Will remove all branches not on remote|
|`--node-modules`|Remove node modules folder|

This command is for cleaning up folders in the workspace or globally. You need to specify at least one option.